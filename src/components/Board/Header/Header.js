import "./Header.css";
import Button from "./Button/Button";
import CircleAvatar from "../../../assets/avatar.png";
import { HiOutlineViewBoards, HiDotsHorizontal} from "react-icons/hi";
import { FiUsers } from "react-icons/fi";
import { FaConciergeBell } from "react-icons/fa";
import { BsStar } from "react-icons/bs";

function Header(props) {
  const verticalDivider = <span className="vertical-divider">|</span>;

  const pushRight = props.showSidebar ? "right" : "";

  return (
    <div className="header">
      <span>
        <Button title="Board" leading={<HiOutlineViewBoards />} />
        <span className="team-name">{props.name}</span>
        <Button leading={<BsStar />} />
        {verticalDivider}
        <Button title="Prairie Health" />
        {verticalDivider}
        <Button title="Team Visible" leading={<FiUsers />} />
        {verticalDivider}
        <img src={CircleAvatar} alt="User's logo" />
        <Button title="Invite" />
      </span>
      <span className={pushRight}>
        <Button title="Butler" leading={<FaConciergeBell />} />
        <Button
          title="Show Menu"
          leading={<HiDotsHorizontal />}
          clickHandler={props.toggleSidebar}
        />
      </span>
    </div>
  );
}

export default Header;
