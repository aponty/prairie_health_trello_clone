import "./Button.css";

function Button(props) {
  let paddingLeftClass = "btn-padding-left";
  if (!(props.leading && props.title)) paddingLeftClass = "";

  return (
    <button
      className="board-header-btn"
      type="button"
      onClick={props.clickHandler}
    >
      {props.leading}
      <span className={paddingLeftClass}>{props.title}</span>
    </button>
  );
}

export default Button;
