import "./Sidebar.css";
import Row from "./Row/Row";
import { RiDashboardFill } from "react-icons/ri";
import { GrClose } from 'react-icons/gr'
import { AiOutlinePicture, AiOutlineSearch, AiOutlineRobot } from "react-icons/ai";
import { BiSticker, BiRocket } from "react-icons/bi";
import { HiDotsHorizontal } from "react-icons/hi";
import {BsListUl} from 'react-icons/bs'

function Sidebar(props) {
  const horizontalDivider = <div className="h-divider"></div>;
  return (
    <div className="sidebar">
      <div className="sidebar-head">
        <h1>Menu</h1>
        <div onClick={props.toggle}>
          <GrClose />
        </div>
      </div>
      {horizontalDivider}
      <Row
        leading={<RiDashboardFill />}
        title="About This Board"
        following="Add a description for your board"
      />
      <Row leading={<AiOutlinePicture />} title="Change Background" />
      <Row leading={<AiOutlineSearch />} title="Search Cards" />
      <Row leading={<BiSticker />} title="Stickers" />
      <Row leading={<HiDotsHorizontal />} title="More" />
      {horizontalDivider}
      <Row
        leading={<AiOutlineRobot />}
        title="Butler"
        following="Automate cards and more"
      />
      {horizontalDivider}
      <Row
        leading={<BiRocket />}
        title="Power-Ups"
        following="Calendar Power-Up, Google Drive and more..."
      />
      {horizontalDivider}
      <Row
        leading={<BsListUl/>}
        title="Activity"
      />
    </div>
  );
}

export default Sidebar;
