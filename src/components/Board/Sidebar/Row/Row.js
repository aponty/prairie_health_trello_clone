import './Row.css'

function Row(props){
    return (
        <div className="sidebar-row" onClick={()=>{console.log('clicked')}}>
            <div className="leading-icon">
                {props.leading}
            </div>
            <div className="sidebar-row-body">
                <div className="row-title">
                    {props.title}
                </div>
                <div className="row-description">
                    {props.following}
                </div>
            </div>
        </div>
    )
}

export default Row;