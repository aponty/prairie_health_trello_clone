import "./Cards.css";
import React from "react";
import { AiOutlineEdit } from "react-icons/ai";
import DeleteMenu from "../../../Partials/DeleteMenu/DeleteMenu";

class Card extends React.Component {
  state = {
    showMenu: false,
  };

  toggleMenu = () => this.setState({ showMenu: !this.state.showMenu });

  render() {
    return (
      <div className="card">
        {this.props.title}
        <DeleteMenu
          icon={<AiOutlineEdit />}
          submitHandler={() =>
            this.props.deleteCard(this.props.listID, this.props.id)
          }
          type="card"
        />
      </div>
    );
  }
}

export default Card;
