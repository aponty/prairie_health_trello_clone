import React from "react";
import "./List.css";
import { HiDotsHorizontal } from "react-icons/hi";
import Card from "./Cards/Cards";
import DeleteMenu from "../../Partials/DeleteMenu/DeleteMenu";
import ExpandingAddInput from "../../Partials/ExpandingAddInput/ExpandingAddInput";

class List extends React.Component {
  state = {
    editingTitle: false,
    title: this.props.name,
  };

  handleTitleChange = (e) => this.setState({ title: e.target.value });

  handleCardSubmit = (title) => this.props.addCard(this.props.id, title);

  handleTitleSubmit = () => this.setState({ editingTitle: false });

  toggleEdit = () => this.setState({ editingTitle: !this.state.editingTitle });

  renderCards = ({ cards }) => {
    return cards.map((card) => (
      <Card
        key={card.id}
        listID={this.props.id}
        deleteCard={this.props.deleteCard}
        {...card}
      />
    ));
  };

  renderLastList = (callback) => {
    return (
      <div className="list-wrapper" tabIndex={0}>
        <div className="list last">
          <ExpandingAddInput
            placeholder="Enter list title..."
            type="list"
            handleSubmit={callback}
          />
        </div>
      </div>
    );
  };

  deleteComp = () => {
    return (
      <DeleteMenu
        icon={<HiDotsHorizontal />}
        submitHandler={() => this.props.deleteList(this.props.id)}
        type="list"
      />
    );
  };

  renderHeader() {
    if (this.state.editingTitle) {
      return (
        <span className="list-title">
          <textarea
            className="list-title-textarea"
            value={this.state.title}
            onClick={(e) => e.stopPropagation()}
            onChange={this.handleTitleChange}
            onBlur={this.handleTitleSubmit}
          >
            {this.state.title}
          </textarea>
          {this.deleteComp()}
        </span>
      );
    }
    return (
      <span className="list-title">
        <h1 onClick={this.toggleEdit}>{this.state.title}</h1>
        {this.deleteComp()}
      </span>
    );
  }

  render() {
    if (this.props.isLast) {
      return this.renderLastList(this.props.handleNewListSubmit);
    };
    
    return (
      <div className="list-wrapper" tabIndex={0}>
        <div className="list">
          {this.renderHeader()}
          {this.renderCards(this.props)}
          <ExpandingAddInput
            placeholder="Enter card title..."
            type="card"
            handleSubmit={this.handleCardSubmit}
          />
        </div>
      </div>
    );
  }
}

export default List;
