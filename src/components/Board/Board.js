import React from "react";
import "./Board.css";
import Header from "./Header/Header";
import List from "./List/List";
import Sidebar from "./Sidebar/Sidebar";

class Board extends React.Component {
  state = {
    showSidebar: false,
  };

  handleNewTitleChange = (e) => this.setState({ newListTitle: e.target.value });

  handleNewListSubmit = (title) => this.props.addList(title);

  toggleSidebar = () => this.setState({ showSidebar: !this.state.showSidebar });

  renderLists = ({ lists }) => {
    return lists.map((list) => (
      <List
        key={list.id}
        addCard={this.props.addCard}
        deleteCard={this.props.deleteCard}
        deleteList={this.props.deleteList}
        {...list}
      />
    ));
  };

  render() {
    return (
      <div className="board-container">
        <Header
          name={this.props.board.name}
          toggleSidebar={this.toggleSidebar}
          showSidebar={this.state.showSidebar}
        />
        <div className="board">
          {this.renderLists(this.props.board)}
          <List isLast={true} handleNewListSubmit={this.handleNewListSubmit} />
          {this.state.showSidebar && <Sidebar toggle={this.toggleSidebar} />}
        </div>
      </div>
    );
  }
}

export default Board;
