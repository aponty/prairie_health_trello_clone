import React from "react";
import "./DeleteMenu.css";

class DeleteMenu extends React.Component {
  state = {
    showMenu: false,
  };

  toggleMenu = () => this.setState({ showMenu: !this.state.showMenu });

  render() {
    return (
      <div className="edit">
        <span onClick={this.toggleMenu}>{this.props.icon}</span>
        {this.state.showMenu && (
          <div className="menu">
            <button onClick={this.props.submitHandler}>
              Delete this {this.props.type}
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default DeleteMenu;
