import React from "react";
import "./ExpandingAddInput.css";
import { GrClose } from "react-icons/gr";
import { HiDotsHorizontal } from "react-icons/hi";

class ExpandingAddInput extends React.Component {
  state = {
    expanded: false,
    inputVal: "",
  };

  toggleVis = () => {
    console.log("vis toggled");
    this.setState({ expanded: !this.state.expanded });
  };

  handleChange = (e) => this.setState({ inputVal: e.target.value });

  handleSubmit = () => {
    this.setState({ expanded: false, inputVal: "" });
    this.props.handleSubmit(this.state.inputVal);
  };

  render() {
    if (this.state.expanded) {
      return (
        <div className="add-list expanded">
          <textarea
            placeholder={this.props.placeholder}
            value={this.state.inputVal}
            onChange={this.handleChange}
          />
          <div className="add-list-following">
            <span>
                <button type="button" onClick={this.handleSubmit}>
                    Add {this.props.type}
                </button>
                <div onClick={this.toggleVis}>
                    <GrClose />
                </div>
            </span>
            <HiDotsHorizontal />
          </div>
        </div>
      );
    }

    return (
      <div className="add-list" onClick={this.toggleVis}>
        + Add another {this.props.type}
      </div>
    );
  }
}

export default ExpandingAddInput;
