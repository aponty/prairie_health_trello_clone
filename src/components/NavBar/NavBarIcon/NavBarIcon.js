import "./NavBarIcon.css";

function NavBarIcon(props) {
  const sgvs = (strings = []) => (
    <svg className="nav-bar-svg">
      {strings.map((string, i) => (
        <path
          key={i}
          fillRule="evenodd"
          clipRule="evenodd"
          fill="currentColor"
          d={string}
        ></path>
      ))}
    </svg>
  );

  return (
    <button className="nav-bar-btn" type="button">
      {props.leading}
      {sgvs(props.svgStrings)}
      {props.trailing}
    </button>
  );
}

export default NavBarIcon;
