import NavBarIcon from "./NavBarIcon/NavBarIcon";
import circleAvatar from "../../assets/avatar.png";
import "./NavBar.css";
// copied over actual svg's & logo from trello just to get the right look
// sticking with library icons for the rest of the app
import ICON_SVGS from "../../assets/icons.json"

function navBar() {
  return (
    <div className="nav-bar">
      <span>
        <NavBarIcon svgStrings={[ICON_SVGS.switch]} />
        <NavBarIcon svgStrings={[ICON_SVGS.home]} />
        <NavBarIcon
          svgStrings={[ICON_SVGS.boards]}
          trailing={<span>Boards</span>}
        />
        <NavBarIcon
          leading={<span>Jump to...</span>}
          svgStrings={[ICON_SVGS.search]}
        />
      </span>

      <span>
        <NavBarIcon svgStrings={[ICON_SVGS.plus]} />
        <NavBarIcon svgStrings={[ICON_SVGS.infoCircle, ICON_SVGS.infoIcon]} />
        <NavBarIcon svgStrings={[ICON_SVGS.notificationIcon]} />
        <img src={circleAvatar} alt="User's logo" />
      </span>
    </div>
  );
}

export default navBar;
