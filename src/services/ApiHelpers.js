import Data from "./data.json";
/**
 * facade around fetch/post etc. Nice for namespacing and single-source-of-truth for authentication tokens
 * in this case, just importing the data and sending it on a delay to simulate a fetch
 */
class apiHelpers {
  static async get(url, query) {
    // form queryString, attach authHeader(), ship it out
    return new Promise((resolve) => setTimeout(resolve, 1000, Data));
  }

  static authHeader() {
    // build/find/update token for current user
  }
}

export default apiHelpers;
