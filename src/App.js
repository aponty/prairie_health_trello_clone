import "./App.css";
import React from "react";
import Board from "./components/Board/Board";
import NavBar from "./components/NavBar/NavBar";
import ApiHelpers from "./services/ApiHelpers";

class App extends React.Component {
  state = {};

  componentDidMount() {
    ApiHelpers.get("some_url", "user_id").then((data) =>
      this.setState({ ...data })
    );
  }

  // as the ids for new items are mocked off arr.length, we could have duplicates after adding and deleting
  // Quick fix to reset id's to ensure uniqueness since they're not originating from a db here
  resetIDs = (arr) => {
    return arr.map((el, i) => {
      el.id = i;
      return el;
    });
  };

  addCard = (listID, newCardTitle) => {
    const newBoard = { ...this.state.board };
    const list = newBoard.lists.filter((list) => list.id === listID)[0];
    list.cards = this.resetIDs(list.cards);
    const newCard = { id: list.cards.length, title: newCardTitle };
    list.cards.push(newCard);
    this.setState({ board: newBoard });
  };

  deleteCard = (listID, cardID) => {
    const newBoard = { ...this.state.board };
    const list = newBoard.lists.filter((list) => list.id === listID)[0];
    list.cards = list.cards.filter((card) => card.id !== cardID);
    this.setState({ board: newBoard });
  };

  addList = (title) => {
    const newBoard = { ...this.state.board };
    newBoard.lists = this.resetIDs(newBoard.lists);
    const newList = { id: newBoard.lists.length, name: title, cards: [] };
    newBoard.lists.push(newList);
    this.setState({ board: newBoard });
  };

  deleteList = (listID) => {
    const newBoard = { ...this.state.board };
    newBoard.lists = newBoard.lists.filter((list) => list.id !== listID);
    this.setState({ board: newBoard });
  };

  renderBoard(board) {
    if (board)
      return (
        <Board
          board={board}
          addCard={this.addCard}
          deleteCard={this.deleteCard}
          addList={this.addList}
          deleteList={this.deleteList}
        />
      );
    return <div>...loading</div>;
  }

  render() {
    return (
      <div className="app">
        <NavBar />
        {this.renderBoard(this.state.board)}
      </div>
    );
  }
}

export default App;
